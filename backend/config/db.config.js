const mysql = require('mysql');

const dbConn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_MYSQL,
    port: process.env.DB_PORT || 3306
});

dbConn.connect(function (err){
    if(err) throw error;
    console.log('Database Connected Successfully!!!');
});

module.exports = dbConn;