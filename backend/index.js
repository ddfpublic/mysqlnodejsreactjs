require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.APP_PORT || 5000;

//Parse request data content x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
//Parse request data content json
app.use(bodyParser.json());

console.log(process.env.APP_PORT);

//Route
app.get('/', (req, res) => {
    res.send("index");
    
});

const employeeRoutes = require('./src/routes/employee.route');
app.use('/api/v1/employee', employeeRoutes);

const productRoutes = require('./src/routes/product.route');
app.use('/api/v1/product', productRoutes);

const userRoutes = require('./src/routes/user.route');
app.use('/api/v1/user', userRoutes);

app.listen(port, () => {
    console.log(`Express server is runing at port ${port}`);
});

