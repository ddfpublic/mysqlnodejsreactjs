const express = require('express');
const router = express.Router();

const employeesController = require('../controllers/employee.controller');
//get all employees
router.get('/', employeesController.getEmployeeList);
//get employees by id
router.get('/:id', employeesController.getEmployeeByID);
//create a new employee
router.post('/', employeesController.createNewEmployee);
//update employee
router.put('/:id', employeesController.updateEmployee);
//delete employee
router.delete('/:id', employeesController.deleteEmployee);

module.exports = router;