const express = require('express');
const router = express.Router();

const usersController = require('../controllers/user.controller');
//get all users
router.get('/', usersController.getUserList);
//get users by id
router.get('/:id', usersController.getUserByID);
//create a new User
router.post('/', usersController.createNewUser);
//update User
router.put('/:id', usersController.updateUser);
//delete User
router.delete('/:id', usersController.deleteUser);
//user login
//router.post('/login', usersController.login);

module.exports = router;