const express = require('express');
const router = express.Router();

const productsController = require('../controllers/product.controller');
//get all products
router.get('/', productsController.getProductList);
//get products by id
router.get('/:id', productsController.getProductByID);
//create a new Product
router.post('/', productsController.createNewProduct);
//update Product
router.put('/:id', productsController.updateProduct);
//delete Product
router.delete('/:id', productsController.deleteProduct);

module.exports = router;