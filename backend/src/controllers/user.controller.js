const UserModel = require('../models/user.model');
//get all User list

exports.getUserList = (req, res) => {
    UserModel.getAllUsers((err, users) => {
        console.log("We are here");
        if(err) {
            res.send(err);
        }else{
            console.log("Users", users);
            res.send(users);
        }
    })
}

//get user by ID
exports.getUserByID = (req, res) => {
    UserModel.getUserByID(req.params.id, (err, user) => {
        if(err)
        res.send(err);
        console.log("Single user data", user);
        res.send(user);
    })
}

//create new user

exports.createNewUser = (req, res) => {
    const userReqData = new UserModel(req.body);
    console.log("userReqData", userReqData);
    //check null user
    if(req.body.contructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        UserModel.createUser(userReqData, (err, user) => {
            if(err)
            res.send(err);
            res.json({status: true, message: 'User Created Successfully', data: user });
        })
    }
}

//update user
exports.updateUser = (req, res) => {
    const userReqData = new UserModel(req.body);
    console.log("userReqData update", userReqData);
    //check null user
    if(req.body.contructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        UserModel.updateUser(req.params.id, userReqData, (err, user) => {
            if(err)
            res.send(err);
            res.json({status: true, message: 'User Updated Successfully'});
            //res.json({status: true, message: 'User Updated Successfully', data: user });
        })
    }
}

//delete user
exports.deleteUser = (req, res) => {
    UserModel.deleteUser(req.params.id, (err, user) => {
        if(err)
        res.send(err);
        res.json({success: true, message: 'User Deleted Successfully'});
    })
}


exports.loginUser = (req, res) => {
    UserModel.loginUser(req.params.id, (err, user) => {
        if(err)
        res.send(err);
        res.json({success: true, message: 'User login Successfully'});
    })
}
