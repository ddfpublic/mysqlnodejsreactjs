const ProductModel = require('../models/product.model');
//get all Product list

exports.getProductList = (req, res) => {
    ProductModel.getAllProducts((err, products) => {
        console.log("We are here");
        if(err) {
            res.send(err);
        }else{
            console.log("Products", products);
            res.send(products);
        }
    })
}

//get product by ID
exports.getProductByID = (req, res) => {
    ProductModel.getProductByID(req.params.id, (err, product) => {
        if(err)
        res.send(err);
        console.log("Single product data", product);
        res.send(product);
    })
}

//create new product

exports.createNewProduct = (req, res) => {
    const productReqData = new ProductModel(req.body);
    console.log("productReqData", productReqData);
    //check null product
    if(req.body.contructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        ProductModel.createProduct(productReqData, (err, product) => {
            if(err)
            res.send(err);
            res.json({status: true, message: 'Product Created Successfully', data: product });
        })
    }
}

//update product
exports.updateProduct = (req, res) => {
    const productReqData = new ProductModel(req.body);
    console.log("productReqData update", productReqData);
    //check null product
    if(req.body.contructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        ProductModel.updateProduct(req.params.id, productReqData, (err, product) => {
            if(err)
            res.send(err);
            res.json({status: true, message: 'Product Updated Successfully'});
            //res.json({status: true, message: 'Product Updated Successfully', data: product });
        })
    }
}

//delete product
exports.deleteProduct = (req, res) => {
    ProductModel.deleteProduct(req.params.id, (err, product) => {
        if(err)
        res.send(err);
        res.json({success: true, message: 'Product Deleted Successfully'});
    })
}

