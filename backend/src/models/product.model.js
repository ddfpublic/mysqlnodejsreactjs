var dbConn = require('../../config/db.config');

var Product = function(product){
    this.name = product.name;
    this.slug = product.slug;
    this.code = product.code;
    this.description = product.description;
    this.content = product.content;
    this.price = product.price;
    this.status = product.status ? product.status : 1;
    this.created_at = new Date();
    this.updated_at = new Date();
}

//get all products
Product.getAllProducts = (result) => {
    dbConn.query('SELECT * FROM products', (err, res) => {
        if(err) {
            console.log("Error while fetching products", err);
            result(null,err);
        }else{
            result(null,res);
        }
    })
}

//get products by id
Product.getProductByID = (id, result) => {
    dbConn.query('SELECT * FROM products WHERE id=?', id, (err, res) => {
        if(err) {
            console.log("Error while fetching product by id", err);
            result(null,err);
        }else{
            result(null,res);

        }
    })

}

// create a new Product
Product.createProduct = (productReqData, result) => {
    dbConn.query('INSERT INTO products SET ?', productReqData, (err, res) => {
        if(err){
            console.log("Error while inserting product");
            result(null,err);
        }else{
            console.log("Product created successfully");
            result(null,res);
        }

    })
}

// update product
Product.updateProduct = (id, productReqData, result) => {
    dbConn.query("UPDATE products SET name=?,slug=?,code=?,description=?,content=?,price=?,status=? WHERE id=?", [productReqData.name,productReqData.slug,productReqData.code,productReqData.description,productReqData.content,productReqData.price,productReqData.status, id], (err, res) => {
        if(err){
            console.log("Error while updating product");
            result(null,err);
        }else{
            console.log("Product updated successfully");
            result(null,res);
        }
    })
}

Product.deleteProduct = (id, result) => {
    // dbConn.query("DELETE FROM products WHERE id=?",[id], (err, res)=>{
    //     if(err){
    //         console.log("Error while deleting product");
    //         result(null,err);
    //     }else{
    //         result(null,res);
    //     }
    // })

    dbConn.query("UPDATE products SET is_deleted=? WHERE id=?", [1, id], (err, res) => {
        if(err){
            console.log("Error while delete product");
            result(null,err);
        }else{
            console.log("Product deleted successfully");
            result(null,res);
        }
    })

}


module.exports = Product;