var dbConn = require('../../config/db.config');

var User = function(user){
    this.email = user.email;
    this.password = user.password;
    this.first_name = user.first_name;
    this.last_name = user.last_name;
    this.address = user.address;
    this.phone = user.phone;
    this.role = user.role;
    this.status = user.status ? user.status : 1;
    this.created_at = new Date();
    this.updated_at = new Date();
}

//get all users
User.getAllUsers = (result) => {
    dbConn.query('SELECT * FROM users', (err, res) => {
        if(err) {
            console.log("Error while fetching users", err);
            result(null,err);
        }else{
            result(null,res);
        }
    })
}

//get users by id
User.getUserByID = (id, result) => {
    dbConn.query('SELECT * FROM users WHERE id=?', id, (err, res) => {
        if(err) {
            console.log("Error while fetching user by id", err);
            result(null,err);
        }else{
            result(null,res);

        }
    })

}

// create a new User
User.createUser = (userReqData, result) => {
    dbConn.query('INSERT INTO users SET ?', userReqData, (err, res) => {
        if(err){
            console.log("Error while inserting user");
            result(null,err);
        }else{
            console.log("User created successfully");
            result(null,res);
        }

    })
}

// update user
User.updateUser = (id, userReqData, result) => {
    dbConn.query("UPDATE users SET email=?,password=?,first_name=?,last_name=?,address=?,phone=?,role=?,status=? WHERE id=?", [userReqData.email,userReqData.password,userReqData.first_name,userReqData.last_name,userReqData.address,userReqData.phone,userReqData.role,userReqData.status, id], (err, res) => {
        if(err){
            console.log("Error while updating user");
            result(null,err);
        }else{
            console.log("User updated successfully");
            result(null,res);
        }
    })
}

User.deleteUser = (id, result) => {
    // dbConn.query("DELETE FROM users WHERE id=?",[id], (err, res)=>{
    //     if(err){
    //         console.log("Error while deleting user");
    //         result(null,err);
    //     }else{
    //         result(null,res);
    //     }
    // })

    dbConn.query("UPDATE users SET is_deleted=? WHERE id=?", [1, id], (err, res) => {
        if(err){
            console.log("Error while delete user");
            result(null,err);
        }else{
            console.log("User deleted successfully");
            result(null,res);
        }
    })

}


module.exports = User;